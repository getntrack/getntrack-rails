# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'getntrack/rails/version'

Gem::Specification.new do |spec|
  spec.name          = "getntrack-rails"
  spec.version       = Getntrack::Rails::VERSION
  spec.authors       = ["Dmitry Kontsevoy"]
  spec.email         = ["dmitry.kontsevoy@gmail.com"]

  spec.summary       = %q{GetNTrack monitoring plugin for Rails}
  spec.license       = "MIT"

  # spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.files         = Dir["{bin,lib}/**/*"]
  spec.files        += ["LICENSE.txt", "Rakefile"]
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'railties', ">= 4.2.4"
  spec.add_dependency 'activesupport', ">= 4.2.4"
  spec.add_dependency 'faraday'

  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "rails", "~> 4.2.0"
  spec.add_development_dependency "capybara"
end
