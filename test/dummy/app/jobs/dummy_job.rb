class DummyJob < ActiveJob::Base
  rescue_from RuntimeError do
  end

  def perform(status=true)
    if status
      sleep 0.1
    else
      raise "Error"
    end
  end
end
