class IntegrationTestCase < ActiveSupport::TestCase
  include Capybara::DSL
  include Rails.application.routes.url_helpers

  protected

  def aggregator
    Getntrack.tracker.aggregator
  end
end
