ENV["RAILS_ENV"] = "test"

require File.expand_path('../dummy/config/environment', __FILE__)
require 'minitest/mock'
require 'rails/test_help'
Rails.backtrace_cleaner.remove_silencers!

require 'capybara/rails'
Capybara.default_driver = :rack_test
Capybara.default_selector = :css

Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

ActiveRecord::Migrator.migrate File.expand_path("../dummy/db/migrate/", __FILE__)
