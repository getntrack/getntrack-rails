module Getntrack::Rails
  class TrackerTest < ActiveSupport::TestCase
    def setup
      @client = Minitest::Mock.new
      @tracker = Tracker.new(@client)
    end

    test "sync" do
      @client.expect :post_metrics, true, [Array]
      @tracker.inc 'rails'
      @tracker.sync
      assert @client.verify
      assert @tracker.aggregator.values.empty?
    end

  end
end
