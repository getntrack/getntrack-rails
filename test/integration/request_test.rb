require 'test_helper'

class RequestTest < IntegrationTestCase
  test "index request" do
    visit '/' 
    assert aggregator['rails.request.count'], 1
    assert_not_nil aggregator['rails.request.duration']
    assert aggregator['rails.request.welcome/index.count'], 1
    assert_not_nil aggregator['rails.request.welcome/index.duration']
  end
end
