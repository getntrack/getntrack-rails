require 'test_helper'

class JobTest < IntegrationTestCase
  test "queue" do
    aggregator.reset
    DummyJob.perform_later
    assert aggregator['rails.job.queued'], 1
    assert aggregator['rails.job.dummy_job.queued'], 1
  end

  test "success job" do
    aggregator.reset
    @job = DummyJob.new
    @job.perform_now
    assert aggregator['rails.job.count'], 1
    assert aggregator['rails.job.dummy_job.count'], 1
    assert aggregator['rails.job.success'], 1
    assert aggregator['rails.job.dummy_job.success'], 1
    assert_not_nil aggregator['rails.job.duration']
    assert_not_nil aggregator['rails.job.dummy_job.duration']
  end

  test "error job" do
    aggregator.reset
    @job = DummyJob.new(false)
    @job.perform_now
    assert_nil aggregator['rails.job.duration']
    assert_nil aggregator['rails.job.success']
    assert aggregator['rails.job.count'], 1
    assert aggregator['rails.job.dummy_job.count'], 1
    assert aggregator['rails.job.error'], 1
    assert aggregator['rails.job.dummy_job.error'], 1
  end
end
