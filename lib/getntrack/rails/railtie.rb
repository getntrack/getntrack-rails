module Getntrack
  module Rails

    class Railtie < ::Rails::Railtie
      config.getntrack = ::ActiveSupport::OrderedOptions.new
      config.getntrack.background_sync_period = 60

      initializer "checkntrack-rails.subscribers" do
        opts = config.getntrack
        next unless opts.enabled
        next unless opts.token

        if opts.action_controller_subscriber
          ActionControllerSubscriber.attach_to :action_controller
        end
        if opts.active_job_subscriber
          ActiveJobSubscriber.attach_to :active_job
        end
      end
    end

  end
end
