module Getntrack
  module Rails

    class ActionControllerSubscriber < Subscriber
      def start_processing(event)
        tracker.inc('rails.request.count')

        action = action_name(event.payload)
        if permitted_actions.try(:include?, action)
          tracker.inc("rails.request.#{action}.count")
        end
      end

      def process_action(event)
        payload = event.payload
        
        if payload[:exception]
          tracker.inc('rails.request.exceptions')
        else
          tracker.push('rails.request.duration', event.duration)
        end
        
        action = action_name(payload)
        if permitted_actions.try(:include?, action)
          if payload[:exception]
            tracker.inc("rails.request.#{action}.exceptions")
          else
            tracker.push("rails.request.#{action}.duration", event.duration)
          end
        end
      end

      protected

      def action_name(payload)
        return if payload[:controller].blank? || payload[:action].blank?

        "#{payload[:controller].sub(/Controller$/, '').underscore}/#{payload[:action]}"
      end
      
      def permitted_actions
        config.action_controller_subscriber
      end
    end

  end
end
