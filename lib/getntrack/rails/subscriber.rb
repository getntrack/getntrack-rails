module Getntrack
  module Rails

    class Subscriber < ::ActiveSupport::Subscriber
      protected
      
      def tracker
        Getntrack.tracker
      end

      def config
        ::Rails.application.config.getntrack
      end
    end

  end
end
