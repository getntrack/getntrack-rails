module Getntrack
  module Rails
    class Tracker
      include Logging

      attr_accessor :aggregator, :scheduller, :client

      delegate :inc, :push, to: :aggregator, allow_nil: true

      def initialize(client)
        self.client = client
        self.aggregator = Aggregator.new
      end

      def start_scheduller(period=60)
        self.scheduller ||= Scheduller.new(period) { sync }
      end

      def sync
        data = aggregator.reset.map do |key, value|
          {
            name: key,
            value: value,
            created_at: Time.now
          }
        end
        client.post_metrics(data) if data.present?
      rescue => e
        logger.error e.to_s
      end
    end
  end
end
