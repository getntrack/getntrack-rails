module Getntrack
  module Rails

    class Scheduller
      attr_accessor :thread, :period
      
      def initialize(period, &block)
        self.period = period
        @proc = block

        self.thread = Thread.new { run }
      end

      protected

      def perform
        @proc.call if @proc
      rescue => e
      end

      def run
        compersated_period = period
        loop do
          sleep(compersated_period) if compersated_period > 0

          start = Time.now
          perform
          stop = Time.now

          compersated_period = period - (stop-start)
        end
      end
    end

  end
end
