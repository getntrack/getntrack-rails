module Getntrack
  module Rails
    class ActiveJobSubscriber < Subscriber
      def enqueue(event)
        payload = event.payload
        job = payload[:job]
        name = job.class.name.underscore

        tracker.inc('rails.job.queued')
        tracker.inc("rails.job.#{name}.queued") if allowed_job?(name)
      end

      def perform_start(event)
        payload = event.payload
        job = payload[:job]
        name = job.class.name.underscore

        tracker.inc('rails.job.count')
        tracker.inc("rails.job.#{name}.count") if allowed_job?(name)
      end

      def perform(event)
        payload = event.payload
        job = payload[:job]
        name = job.class.name.underscore

        if payload[:exception]
          tracker.inc('rails.job.error')
          tracker.inc("rails.job.#{name}.error") if allowed_job?(name)
        else
          tracker.inc('rails.job.success')
          tracker.inc("rails.job.#{name}.success") if allowed_job?(name)
          tracker.push('rails.job.duration', event.duration)
          tracker.push("rails.job.#{name}.duration", event.duration) if allowed_job?(name)
        end
      end

      protected

      def allowed_job?(job)
        permitted_actions.try(:include?, job)
      end

      def permitted_actions
        config.active_job_subscriber
      end
    end
  end
end

