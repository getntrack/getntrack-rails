require_relative 'rails/version'
require_relative 'rails/railtie' if defined?(Rails)

module Getntrack
  autoload :Client, 'getntrack/client'

  module Rails
    autoload :Aggregator, 'getntrack/rails/aggregator'
    autoload :Tracker, 'getntrack/rails/tracker'
    autoload :Scheduller, 'getntrack/rails/scheduller'
    autoload :Subscriber, 'getntrack/rails/subscriber'
    autoload :ActionControllerSubscriber, 'getntrack/rails/action_controller_subscriber'
    autoload :ActiveJobSubscriber, 'getntrack/rails/active_job_subscriber'
    autoload :Logging, 'getntrack/rails/logging'

    def self.logger
      @logger ||= ::Rails.logger
    end
    cattr_writer :logger
  end
 
  def self.tracker
    @tracker ||= 
      begin
        opts = ::Rails.application.config.getntrack
        client = nil
        if opts.enabled && opts.token
          client = Getntrack::Client.new(opts.token)
          client.api_endpoint = opts.api_endpoint if opts.api_endpoint
        end
        tmp = Rails::Tracker.new(client)
        tmp.start_scheduller(opts.background_sync_period) if opts.background_sync
        tmp
      end
  end

  def self.tracker=(value)
    @tracker = value
  end

end
